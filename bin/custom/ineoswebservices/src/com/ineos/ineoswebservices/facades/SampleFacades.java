/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.ineos.ineoswebservices.facades;

import de.hybris.platform.core.servicelayer.data.SearchPageData;

import java.util.List;

import com.ineos.ineoswebservices.data.UserData;
import com.ineos.ineoswebservices.dto.SampleWsDTO;
import com.ineos.ineoswebservices.dto.TestMapWsDTO;


public interface SampleFacades
{
	SampleWsDTO getSampleWsDTO(final String value);

	UserData getUser(String id);

	List<UserData> getUsers();

	SearchPageData<UserData> getUsers(SearchPageData<?> params);

	TestMapWsDTO getMap();
}

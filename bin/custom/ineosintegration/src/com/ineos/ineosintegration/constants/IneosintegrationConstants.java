/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.ineos.ineosintegration.constants;

/**
 * Global class for all Ineosintegration constants. You can add global constants for your extension into this class.
 */
public final class IneosintegrationConstants extends GeneratedIneosintegrationConstants
{
	public static final String EXTENSIONNAME = "ineosintegration";

	private IneosintegrationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "ineosintegrationPlatformLogo";
}

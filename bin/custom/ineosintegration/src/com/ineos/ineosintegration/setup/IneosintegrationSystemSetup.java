/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.ineos.ineosintegration.setup;

import static com.ineos.ineosintegration.constants.IneosintegrationConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.ineos.ineosintegration.constants.IneosintegrationConstants;
import com.ineos.ineosintegration.service.IneosintegrationService;


@SystemSetup(extension = IneosintegrationConstants.EXTENSIONNAME)
public class IneosintegrationSystemSetup
{
	private final IneosintegrationService ineosintegrationService;

	public IneosintegrationSystemSetup(final IneosintegrationService ineosintegrationService)
	{
		this.ineosintegrationService = ineosintegrationService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		ineosintegrationService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return IneosintegrationSystemSetup.class.getResourceAsStream("/ineosintegration/sap-hybris-platform.png");
	}
}

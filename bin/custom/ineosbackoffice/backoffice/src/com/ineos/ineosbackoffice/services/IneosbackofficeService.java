/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.ineos.ineosbackoffice.services;

/**
 * Hello World IneosbackofficeService
 */
public class IneosbackofficeService
{
	public String getHello()
	{
		return "Hello";
	}
}

/*
 * Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved
 */
package com.ineos.ineosbackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class IneosbackofficeConstants extends GeneratedIneosbackofficeConstants
{
	public static final String EXTENSIONNAME = "ineosbackoffice";

	private IneosbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
